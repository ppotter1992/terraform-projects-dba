  ## MUST CONFIGURATION ##
  #Parameter group options
  #name            = ""
  #description     = ""
  family           = "postgres11"
  parameters = [
  # Values of the parameters
 {
   name = "timezone"
   value = "Navajo"
   apply_method = "immediate"
 }
 ,
 {
   name = "search_path"
   value = "$user,public,snapdata,snapanalytics"
   apply_method = "immediate"
 }
]
  
