  ## MUST CONFIGURATION ##
  #Parameter group options
  #name            = ""
  #description     = ""
  family           = "aurora-postgresql12"
  parameters = [
  # Values of the parameters
 {
   name = "search_path"
   value = "$user,public,snapdata,snapanalytics"
   apply_method = "immediate"
 }
]
  
