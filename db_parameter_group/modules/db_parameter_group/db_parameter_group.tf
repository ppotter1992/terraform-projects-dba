provider "aws" {
  region = var.region
}

terraform {

   backend "s3" {}

}

resource "aws_db_parameter_group" "db_parameter_group" {

  name        = var.name
  description = var.description
  family      = var.family

  dynamic "parameter" {
    for_each = var.parameters
    content {
      name         = parameter.value.name
      value        = parameter.value.value
      apply_method = lookup(parameter.value, "apply_method", null)
    }
  }

  tags = merge(
    var.tags,
    {
      "Name" = var.name
    },
  )

  lifecycle {
    create_before_destroy = true
  }
}

output "db_parameter_group_output" {
  value = aws_db_parameter_group.db_parameter_group.name
}