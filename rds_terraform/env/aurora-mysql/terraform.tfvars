  region                                    = "us-west-2"
  #aws_rds_cluster
  cluster_identifier                        = "aurora-cluster"
  engine                                    = "aurora-mysql"
  engine_version                            = "5.7.mysql_aurora.2.09.2"
  engine_mode                               = "provisioned"
  allow_major_version_upgrade               = false
  #kms_key_id                                = var.kms_key_id
  database_name                             = "example_db"
  master_username                           = "admin"
  skip_final_snapshot                       = true
  deletion_protection                       = false
  backup_retention_period                   = 7
  preferred_backup_window                   = "03:00-04:00"
  preferred_maintenance_window              = "Sat:00:00-Sat:03:00"
  port                                      = 3344
  db_subnet_group_name                      = "default"
  vpc_security_group_ids                    = ["sg-3e584211"]
  storage_encrypted                         = true
  apply_immediately                         = false
  db_cluster_parameter_group_name           = "aurora-base-mysql-57"
  iam_database_authentication_enabled       = false
  backtrack_window                          = 0
  copy_tags_to_snapshot                     = true

  #aws_rds_cluster_instance
  identifier                            = "aurora-instance"
  instance_class                        = "db.r5.2xlarge"
  publicly_accessible                   = false
  db_parameter_group_name               = "aurora-base-mysql-57-inst"
  auto_minor_version_upgrade            = false
  performance_insights_enabled          = true
#  performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = 7
  ca_cert_identifier                    = "rds-ca-2019"
  tags = {
    Owner       = "dba"
    Environment = "it-devops-test"
  }
