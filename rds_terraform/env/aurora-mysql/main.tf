provider "aws" {
  region = var.region
}

terraform {
   backend "s3" {}
}

locals {
  region = var.region
}

provider "random" { }

resource "random_password" "password" {
  length           = 16
  special          = false
}

output "password" {
  description = "The password is:" 
  value = random_password.password.*.result
  sensitive = true
}

#resource "aws_rds_global_cluster" "global" {
#  global_cluster_identifier = var.global_cluster_identifier
#  engine                    = var.engine
#  engine_version            = var.engine_version
#  database_name             = var.database_name
#}

resource "aws_rds_cluster" "primary" {

#  global_cluster_identifier                 = var.aws_rds_global_cluster.global.global_cluster_identifier
  cluster_identifier                        = var.cluster_identifier
  engine                                    = var.engine
  engine_version                            = var.engine_version
  engine_mode                               = var.engine_mode
  allow_major_version_upgrade               = var.allow_major_version_upgrade
#  kms_key_id                                = var.kms_key_id
  database_name                             = var.database_name
  master_username                           = var.master_username
  master_password                           = random_password.password.result
  skip_final_snapshot                       = var.skip_final_snapshot
  deletion_protection                       = var.deletion_protection
  backup_retention_period                   = var.backup_retention_period
  preferred_backup_window                   = var.preferred_backup_window
  availability_zones                        = ["${local.region}a","${local.region}b"]
  preferred_maintenance_window              = var.preferred_maintenance_window
  port                                      = var.port
  db_subnet_group_name                      = var.db_subnet_group_name
  vpc_security_group_ids                    = var.vpc_security_group_ids
  storage_encrypted                         = var.storage_encrypted
  apply_immediately                         = var.apply_immediately
  db_cluster_parameter_group_name           = var.db_cluster_parameter_group_name
  iam_database_authentication_enabled       = var.iam_database_authentication_enabled
  backtrack_window                          = var.backtrack_window
  copy_tags_to_snapshot                     = var.copy_tags_to_snapshot
}

resource "aws_rds_cluster_instance" "primary" {
  count                                 = 2
  identifier                            = "${var.identifier}-${count.index}"
  cluster_identifier                    = aws_rds_cluster.primary.cluster_identifier
  engine                                = aws_rds_cluster.primary.engine
  engine_version                        = aws_rds_cluster.primary.engine_version
  instance_class                        = var.instance_class
  publicly_accessible                   = var.publicly_accessible
  db_subnet_group_name                  = aws_rds_cluster.primary.db_subnet_group_name
  db_parameter_group_name               = var.db_parameter_group_name
  apply_immediately                     = aws_rds_cluster.primary.apply_immediately
  preferred_maintenance_window          = aws_rds_cluster.primary.preferred_maintenance_window
  auto_minor_version_upgrade            = var.auto_minor_version_upgrade
  performance_insights_enabled          = var.performance_insights_enabled
 # performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period
  copy_tags_to_snapshot                 = aws_rds_cluster.primary.copy_tags_to_snapshot
  ca_cert_identifier                    = var.ca_cert_identifier
}