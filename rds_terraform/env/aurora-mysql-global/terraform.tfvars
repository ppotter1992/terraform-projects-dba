  region                                    = "us-west-2"
  region_primary                            = "us-west-2"
  region_secondary                          = "us-east-1"
  #aws_rds_global_cluster
  global_cluster_identifier                 = "aurora-global"
  engine                                    = "aurora-mysql"
  engine_version                            = "5.7.mysql_aurora.2.09.2"
  database_name                             = "global_db"
  storage_encrypted                         = true
  
  #aws_rds_cluster
  cluster_identifier_primary                = "primary-cluster"
  cluster_identifier_secondary              = "secondary-cluster"
  engine_mode                               = "provisioned"
  allow_major_version_upgrade               = false
  master_username                           = "admin"
  skip_final_snapshot                       = true
  deletion_protection                       = false
  backup_retention_period                   = 7
  preferred_backup_window                   = "03:00-04:00"
  preferred_maintenance_window              = "Sat:00:00-Sat:03:00"
  port                                      = 3344
  db_subnet_group_name                       = ["default","test"]
  vpc_security_group_ids                     = ["sg-3e584211","sg-18f0ce2b"]
  kms_key_id                                 = [
    "arn:aws:kms:us-west-2:414287561050:key/7a5c726c-f8ba-4feb-a4e9-4f4a5922e6de",
    "arn:aws:kms:us-east-1:414287561050:key/6cf73051-4f04-4cbe-a0d4-b7c6e071ebd8"
    ]
  apply_immediately                         = false
  db_cluster_parameter_group_name           = "aurora-base-mysql-57"
  iam_database_authentication_enabled       = false
  backtrack_window                          = 0
  copy_tags_to_snapshot                     = true

  #aws_rds_cluster_instance
  identifier_primary                    = "primary-cluster-instance"
  identifier_secondary                  = "secondary-cluster-instance"
  instance_class                        = "db.r5.2xlarge"
  publicly_accessible                   = false
  db_parameter_group_name               = "aurora-base-mysql-57-inst"
  auto_minor_version_upgrade            = false
  performance_insights_enabled          = true
#  performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = 7
  ca_cert_identifier                    = "rds-ca-2019"
  tags = {
    Owner       = "dba"
    Environment = "it-devops-test"
  }
