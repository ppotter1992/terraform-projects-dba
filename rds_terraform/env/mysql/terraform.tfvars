  ## MUST CONFIGURATION ##
  #Engine options
  engine            = "mysql"
  engine_version    = "8.0.23"

  #DB instance class
  instance_class    = "db.t3.medium"

  #Storage
  storage_type      = "io1"
  allocated_storage = 125
  iops              = 1000
  
  #Connectivity
  vpc_security_group_ids = ["sg-3e584211"]
  db_subnet_group_name = "default"
  publicly_accessible = false

  #Database authentication
  iam_database_authentication_enabled = false
  
  #Additional configuration
  parameter_group_name = "demoinst-20211001141932729200000001"
  option_group_name = "default:mysql-8-0"
  backup_retention_period = 7
  maintenance_window = "Sat:00:00-Sat:03:00"
  backup_window      = "03:00-04:00"
  copy_tags_to_snapshot = true
  tags = {
    Owner       = "user1"
    Environment = "prodtest1"
  }
  
  auto_minor_version_upgrade = false
  allow_major_version_upgrade = false
  deletion_protection = false
  apply_immediately = true
  ca_cert_identifier = "rds-ca-2019"
  skip_final_snapshot = true

  multi_az = true

  #KMS
  storage_encrypted = true
  #kms_key_id
  #Performance Insights
  performance_insights_enabled = true
  performance_insights_retention_period = 7

  #USER INPUT
  #identifier = "demoinst2"
  #name     = "demodb2"
  #username = "user"
  #password = "YourPwdShouldBeLongAndSecure!"
  #port     = "3344"
