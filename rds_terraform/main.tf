provider "aws" {
  region = var.region
}

terraform {
   backend "s3" {}
}

#module "rds" {
resource "aws_db_instance" "rds" {
  identifier = var.identifier

  engine            = var.engine
  engine_version    = var.engine_version
  instance_class    = var.instance_class
  storage_type      = var.storage_type
  iops              = var.iops
  allocated_storage = var.allocated_storage
  

  name     = var.name
  username = var.username
  password = var.password
  port     = var.port

  iam_database_authentication_enabled = var.iam_database_authentication_enabled

  vpc_security_group_ids = var.vpc_security_group_ids

  maintenance_window = var.maintenance_window
  backup_window      = var.backup_window

  allow_major_version_upgrade = var.allow_major_version_upgrade

  apply_immediately = var.apply_immediately 
  publicly_accessible = var.publicly_accessible
  ca_cert_identifier = var.ca_cert_identifier

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  #monitoring_interval = "30"
  #monitoring_role_name = "MyRDSMonitoringRole"
  #create_monitoring_role = true

  tags = var.tags

  copy_tags_to_snapshot = var.copy_tags_to_snapshot

  # DB subnet group
  #subnet_ids = ["subnet-4864bf12", "subnet-d216d212"]
  db_subnet_group_name = var.db_subnet_group_name
  #subnet_ids = ["subnet-d216d298"]
   
  # DB parameter group
  parameter_group_name = var.parameter_group_name

  # DB option group
  option_group_name = var.option_group_name
  
  # Database Deletion Protection
  deletion_protection = var.deletion_protection
  
  # BACKUP RETENTION
  backup_retention_period = var.backup_retention_period
  
  #KMS
  storage_encrypted = var.storage_encrypted
  
  #Performance Insights
  performance_insights_enabled = var.performance_insights_enabled
  performance_insights_retention_period = var.performance_insights_retention_period
  
  auto_minor_version_upgrade = var.auto_minor_version_upgrade
  multi_az = var.multi_az
  skip_final_snapshot = var.skip_final_snapshot
}